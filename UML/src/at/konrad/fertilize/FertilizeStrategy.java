package at.konrad.fertilize;

public interface FertilizeStrategy {
	public void doFertilize();
}
