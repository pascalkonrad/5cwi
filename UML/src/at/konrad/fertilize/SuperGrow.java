package at.konrad.fertilize;

public class SuperGrow implements FertilizeStrategy {

	@Override
	public void doFertilize() {
		System.out.println("Fertilize with Super Grow");
		
	}

}
