package at.konrad.observable;
import java.util.ArrayList;
import java.util.List;

public class Sensor {

	private List<Observable> observables;
	
	public Sensor() {
		super();
		
		this.observables = new ArrayList<Observable>();
	}
	
	public void addObservables(Observable os) {
		
		observables.add(os);
		
		
	}
	
	public void informAll(){
		for (Observable observable : observables) {
			observable.inform();
		}
	}	
}
	

