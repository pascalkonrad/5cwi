package at.konrad.football;

import java.util.Date;

public class Fußballspieler {
  public Date Geburtsdatum;
  public int id;
  public int Wert;
  public String Name;
  public Fußballspieler(Date geburtsdatum, int id, int wert, String name) {
    super();
    Geburtsdatum = geburtsdatum;
    this.id = id;
    Wert = wert;
    Name = name;
  }

public Date getGeburtsdatum() {
    return Geburtsdatum;
  }
  public void setGeburtsdatum(Date geburtsdatum) {
    Geburtsdatum = geburtsdatum;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public int getWert() {
    return Wert;
  }
  public void setWert(int wert) {
    Wert = wert;
  }
  public String getName() {
    return Name;
  }
  public void setName(String name) {
    Name = name;
  }
  
}
