package at.konrad.football;

import java.util.Date;

public class Tormann extends Fußballspieler {
  public int Reaktionszeit;


  public Tormann(Date geburtsdatum, int id, int wert, String name, int reaktionszeit) {
    super(geburtsdatum, reaktionszeit, reaktionszeit, name);
    Reaktionszeit = reaktionszeit;
    // TODO Auto-generated constructor stub
  }

  public int getReaktionszeit() {
    return Reaktionszeit;
  }

  public void setReaktionszeit(int reaktionszeit) {
    Reaktionszeit = reaktionszeit;
  }
  

}