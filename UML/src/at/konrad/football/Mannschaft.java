package at.konrad.football;

import java.util.ArrayList;
import java.util.List;

public class Mannschaft {
  public int id;
  public String name;
  public Fußballspieler fußballpieler;
  public List<Fußballspieler> players;

  public Mannschaft(int id, String name) {
    super();
    this.id = id;
    this.name = name;
    this.players = new ArrayList<Fußballspieler>();
  }
  
  public void playerAdd(Fußballspieler player){
    players.add(player);
  }
  

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public void getValueOfTeam() {
  return;
  }

}
